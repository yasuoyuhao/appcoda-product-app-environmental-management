//
//  ServicesURL.swift
//  product-app-env-demo
//
//  Created by yuhao Chen on 2020/1/23.
//  Copyright © 2020 yuhao Chen. All rights reserved.
//

import Foundation

public struct ServicesURL {
    
    static var baseurl: String {
        return (Bundle.main.infoDictionary?["API_URL"] as? String)?.replacingOccurrences(of: "\\", with: "") ?? ""
    }
}
