//
//  ContentView.swift
//  product-app-env-demo
//
//  Created by yuhao Chen on 2020/1/23.
//  Copyright © 2020 yuhao Chen. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text(ServicesURL.baseurl)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
